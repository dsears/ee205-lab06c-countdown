///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   
//
// @author Dane Sears <dsears@hawaii.edu>
// @date   23_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<time.h>
#include<unistd.h>
#include<stdbool.h>

int x = 1;
struct tm bday;

int main(){ 

   time_t seconds;
   int secondsSince = 0;
   int minutesSince;
   int hoursSince;
   int daysSince;
   int yearsSince;

  bday.tm_sec=5;
  bday.tm_min=20;
  bday.tm_mday=26;
  bday.tm_mon=3;
  bday.tm_year=91;
  bday.tm_hour=16;

  time_t reference = mktime(&bday);

  printf("Reference time: %s\n",asctime(&bday));
   while(x == 1){
      sleep(1);
      time_t now = time(0);
   seconds = difftime(reference, now);
   
      yearsSince = seconds/31536000;
      seconds = seconds%31536000;
      daysSince = seconds/86400;
      seconds = seconds%86400;
      hoursSince = seconds/3600;
      seconds = seconds%3600;
      minutesSince = seconds/60;
      secondsSince = seconds%60;

printf("Years: %d  Days: %d, Hours: %d  Minutes: %d  Seconds: %d\n", -yearsSince, -daysSince, -hoursSince, -minutesSince, -secondsSince);
   }
   return 0;
   }
